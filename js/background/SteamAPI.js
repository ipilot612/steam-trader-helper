
export default function SteamAPI () {
	this.accountIsAuthed = false;
	this.accountSteamid = null;
	this.accountApiKey = null;
}

import Utils from './Utils.js';

const utils = new Utils();

SteamAPI.prototype.tryGetAuthedSteamid = async function() {
	let steamid = null;
	let response = await fetch('https://steamcommunity.com');
	let text = await response.text(); 
	let matches = text.match(/g_steamID = "([0-9]+)";/);
	
	if(matches != null) {
		this.accountIsAuthed = true;
		this.accountSteamid = matches[1];
	}
	else {
		this.accountIsAuthed = false;
		this.accountSteamid = null;
	}
	return this.accountSteamid;
};

SteamAPI.prototype.isApiKeyRegistered = async function() {
	let response = await fetch('https://steamcommunity.com/dev/apikey');
	return parseApiKey(response, this);
};

async function parseApiKey(response, self) {
	let text = await response.text();
	let matches = text.match(/<p>.+: ([A-Z0-9]+)<\/p>/);
	if(matches != null) {
		self.accountApiKey = matches[1];
		return true;
	}
	else
		return false;
}

SteamAPI.prototype.isAccountAuthorized = async function() {
	let steamid = await this.tryGetAuthedSteamid();
	return steamid != null;
};

SteamAPI.prototype.createTradeOffer = async function(data) {
	let sessionidCookie = await utils.getCookie('sessionid');

	let fd = new FormData();

	fd.set('sessionid', sessionidCookie.value);
	fd.set('serverid', 1);
	fd.set('partner', data.partnerSteamid);
	fd.set('json_tradeoffer', data.jsonTradeOffer);
	fd.set('trade_offer_create_params', data.tradeOfferCreateParams);
	fd.set('tradeoffermessage', data.tradeOfferMessage);

	let params = utils.urlencodeFormData(fd);
	
	let url = 'https://steamcommunity.com/tradeoffer/new/send';
	
	let response = await fetch(url, {
		method: 'POST',
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded'
		},
		body: params
	});

	if(response.ok === false) {
		return null;
	}

	let json = await response.json();
	json.assetid = data.assetid;
	json.message = data.tradeOfferMessage;

	return json;
};

SteamAPI.prototype.getTradeOffer = async function(tradeOfferId) {
	let url = 'https://api.steampowered.com/IEconService/GetTradeOffer/v1/?tradeofferid='+tradeOfferId+'&language=en&key='+this.accountApiKey;
	let response = await fetch(url);
	return await response.json();
};

SteamAPI.prototype.registerApiKey = async function() {
	let sessionidCookie = await utils.getCookie('sessionid');

	let fd = new FormData();

	fd.set('domain', 'localhost');
	fd.set('agreeToTerms', 'agreed');
	fd.set('sessionid', sessionidCookie.value);
	fd.set('Submit', 'Зарегистрировать');

	let params = utils.urlencodeFormData(fd);
	
	let url = 'https://steamcommunity.com/dev/registerkey';
	
	let response = await fetch(url, {
		method: 'POST',
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded'
		},
		body: params
	});

	if(response.ok === false) {
		return false;
	}

	return parseApiKey(response, this);
};